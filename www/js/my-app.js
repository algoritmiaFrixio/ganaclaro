// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
    swipeBackPage: false,
    swipePanelOnlyClose: true,
    pushState: true,
    template7Pages: true
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: false,
});
var subnaview = myApp.addView('.view-subnav');


$(document).ready(function () {
    $("#RegisterForm").validate();
    $("#LoginForm").validate();
    $("#ForgotForm").validate();
    $(".close-popup").click(function () {
        $("label.error").hide();
    });
    $('.close_info_popup').click(function (e) {
        $('.info_popup').fadeOut(500);
    });
});


$$(document).on('pageInit', function (e) {
    $("#RegisterForm").validate();
    $("#LoginForm").validate();
    $("#ForgotForm").validate();
    $(".close-popup").click(function () {
        $("label.error").hide();
    });


})
myApp.onPageInit('acount', function (page) {
    if (localStorage.getItem('data') === null) {
        location.href = 'index.html';
    }

    function update() {
        const data = btoa(JSON.stringify({
            nombre: $("#nameperfil").val(),
            telefono: $("#phoneperfil").val(),
            telefono_local: $("#phonelocalperfil").val(),
            ciudad: $("#cityperfil").val(),
            local: $("#localperfil").val(),
            departamento: $("#stateperfil").val(),
            barrio: $("#barrioperfil").val(),
            direccion: $("#addressperfil").val(),
            contrasena: $("#passwordperfil").val(),
            id: datos.user.id,
        }));
        $.ajax({
            method: 'PUT',
            contentType: 'application/json; charset=utf-8',
            url: `https://api.clarohogartv.com/api/users/${datos.user.id}?data=${data}`,
            data: data,
            dataType: 'json'
        }).done(function (data, textStatus, jqXHR) {
            if (textStatus === "success") {
                // data.message,

            }
            data = {user: data.user}
            localStorage.setItem('data', btoa(JSON.stringify(data)));
            setTimeout(() => {
                location.reload();
            }, 3500)
        })
            .fail(function (jqXHR, textStatus, errorThrown) {
                toastr(jqXHR.responseJSON.message, 'Error', 'error');
            });
    }

    $("#nameperfil").val(datos.user.nombre);
    $("#phoneperfil").val(datos.user.telefono);
    $("#phonelocalperfil").val(datos.user.telefono_local);
    $("#cityperfil").val(datos.user.ciudad);
    $("#localperfil").val(datos.user.local);
    $("#stateperfil").val(datos.user.departamento);
    $("#barrioperfil").val(datos.user.barrio);
    $("#addressperfil").val(datos.user.direccion);
    $("#passwordperfil").val('*******');
    $('#actualizar').click((e) => {
        e.preventDefault();
        update();
    });
})
