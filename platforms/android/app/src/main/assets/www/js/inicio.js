if (localStorage.getItem('data') === null) {
    location.href = 'index.html';
}
const name = $('#name');
const datos = JSON.parse(atob(localStorage.getItem('data')));
name.html(datos.user.nombre);
$.ajax({
    method: 'GET',
    contentType: 'application/json; charset=utf-8',
    url: `https://api.clarohogartv.com/api/services`,
    dataType: 'json'
}).done(function (data, textStatus, jqXHR) {
    if (textStatus === "success") {
        console.log(data)
    }

})
    .fail(function (jqXHR, textStatus, errorThrown) {
        toastr(jqXHR.responseJSON.message, 'Error', 'error');
    });

const logout = $("#logout");
logout.click(() => {
    localStorage.removeItem('data');
    location.href = 'index.html';
});