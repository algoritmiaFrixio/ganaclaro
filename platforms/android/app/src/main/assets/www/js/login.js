function login() {
    let telefono = $('#telefono').val();
    let contrasena = $('#contrasena').val();
    const data = JSON.stringify({telefono: telefono, contrasena: contrasena});
    $.ajax({
        method: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'https://api.clarohogartv.com/api/auth',
        data: data,
        dataType: 'json'
    }).done(function (data, textStatus, jqXHR) {
        if (textStatus === "success") {
            localStorage.setItem('data', btoa(JSON.stringify(data)));
            toastr(jqXHR.responseJSON.message, 'Exitoso', 'success');
            location.href = 'index-2.html';
        }
    })
        .fail(function (jqXHR, textStatus, errorThrown) {
            toastr(jqXHR.responseJSON.message, 'Error', 'error');
        });
}

const content = $('#content');
const content2 = $('#content2');
content.show();
content2.hide();
setTimeout(() => {
    if (localStorage.getItem('data') !== null) {
        location.href = 'index-2.html';
    } else {
        content.hide();
        content2.show();
        $('#submit').click((e) => {
            e.preventDefault();
            login();
        });
    }
}, 2500);

